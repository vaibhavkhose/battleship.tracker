﻿using Battleship.Api.Tracker.Dtos;
using Battleship.Api.Tracker.Entities;
using Battleship.Api.Tracker.Exeptions;
using Battleship.Api.Tracker.Repositories;
using Battleship.Api.Tracker.Services;
using FluentAssertions;
using NSubstitute;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Battleship.Api.Tracker.UnitTests.Services
{
    public class BoardServiceTests
    {
        private readonly IBoardRepository _boardRepository;
        private const int _playerId = 1;
        private Guid _boardId = Guid.NewGuid();
        private Guid? _shipId = Guid.NewGuid();

        public BoardServiceTests()
        {
            _boardRepository = Substitute.For<IBoardRepository>();
        }

        [Fact]
        public async Task create_board_should_create_board_if_not_exists()
        {
            _boardRepository.CreateBoard(_playerId).Returns(Task.FromResult(_boardId));
            _boardRepository.GetBoard(_playerId).Returns(Task.FromResult((Board)null));

            var boardService = new BoardService(_boardRepository);
            var result = await boardService.CreateBoard(_playerId);
            result.Should().Be(_boardId);
            await _boardRepository.Received(1).CreateBoard(_playerId);
            await _boardRepository.Received(1).GetBoard(_playerId);
        }

        [Fact]
        public async Task create_board_should_throw_board_already_exists_excpetion()
        {
            _boardRepository.CreateBoard(_playerId).Returns(Task.FromResult(_boardId));
            _boardRepository.GetBoard(_playerId).Returns(Task.FromResult(new Board(_playerId)));

            var boardService = new BoardService(_boardRepository);
            Func<Task> createAction = async () => await boardService.CreateBoard(_playerId);
            createAction.Should().Throw<BoardAlreadyExistsException>();
            await _boardRepository.Received(0).CreateBoard(_playerId);
            await _boardRepository.Received(1).GetBoard(_playerId);
        }

        [Theory]
        [InlineData(1, 5, 3, Direction.Horizontal)]
        [InlineData(3, 1, 5, Direction.Vertial)]
        public async Task add_battleship_should_add_valid_battleship_if_not_exists(int startRow, int startColumn, int width, Direction direction)
        {
            var board = new Board(_playerId);
            _boardRepository.AddBattleShip(_playerId, Arg.Any<BattleShip>()).Returns(Task.FromResult(_shipId));
            _boardRepository.GetBoard(_playerId).Returns(Task.FromResult(board));
            var addBattleshipRequest = new AddBattleShipRequest { Direction = direction, StartRow = startRow, StartColumn = startColumn, Width = width };
            
            var boardService = new BoardService(_boardRepository);
            var result = await boardService.AddBattleship(_playerId, addBattleshipRequest);
            result.Should().Be(_shipId.Value);
            await _boardRepository.ReceivedWithAnyArgs(1).AddBattleShip(_playerId, Arg.Any<BattleShip>());
            await _boardRepository.Received(1).GetBoard(_playerId);
        }

        [Fact]
        public async Task add_battleship_should_throw_board_not_found_if_no_board()
        {
            _boardRepository.GetBoard(_playerId).Returns(Task.FromResult((Board)null));
            var addBattleshipRequest = new AddBattleShipRequest { Direction = Direction.Horizontal, StartRow = 1, StartColumn = 1, Width = 3 };

            var boardService = new BoardService(_boardRepository);
            Func<Task> addAction = async () => await boardService.AddBattleship(_playerId, addBattleshipRequest);
            addAction.Should().Throw<BoardDoesNotExistsException>();
            await _boardRepository.ReceivedWithAnyArgs(0).AddBattleShip(_playerId, Arg.Any<BattleShip>());
            await _boardRepository.Received(1).GetBoard(_playerId);
        }

        [Fact]
        public async Task add_battleship_should_throw_ship_already_exists()
        {
            var board = new Board(_playerId);
            board.Battleships.Add(new BattleShip { Position = new Position { StartRow = 1, EndRow = 1, StartColumn = 3, EndColumn = 6 } });
            _boardRepository.GetBoard(_playerId).Returns(board);
            var addBattleshipRequest = new AddBattleShipRequest { Direction = Direction.Horizontal, StartRow = 1, StartColumn = 1, Width = 3 };

            var boardService = new BoardService(_boardRepository);
            Func<Task> addAction = async () => await boardService.AddBattleship(_playerId, addBattleshipRequest);
            addAction.Should().Throw<ShipAlreadyExistsException>();
            await _boardRepository.ReceivedWithAnyArgs(0).AddBattleShip(_playerId, Arg.Any<BattleShip>());
            await _boardRepository.Received(1).GetBoard(_playerId);
        }

        [Fact]
        public async Task add_battleship_should_throw_board_not_found_if_battleship_cant_be_added()
        {
            _boardRepository.GetBoard(_playerId).Returns(Task.FromResult(new Board(_playerId)));
            var addBattleshipRequest = new AddBattleShipRequest { Direction = Direction.Horizontal, StartRow = 1, StartColumn = 1, Width = 3 };
            _boardRepository.AddBattleShip(_playerId, Arg.Any<BattleShip>()).Returns(Task.FromResult((Guid?)null));

            var boardService = new BoardService(_boardRepository);
            Func<Task> addAction = async () => await boardService.AddBattleship(_playerId, addBattleshipRequest);
            addAction.Should().Throw<BoardDoesNotExistsException>();
            await _boardRepository.ReceivedWithAnyArgs(1).AddBattleShip(_playerId, Arg.Any<BattleShip>());
            await _boardRepository.Received(1).GetBoard(_playerId);
        }

        [Theory]
        [InlineData(2, 3, true)]
        [InlineData(5, 6, false)]
        public async Task attack_should_attack(int row, int column, bool expectedHit)
        {
            var board = new Board(_playerId);
            board.Battleships.Add(new BattleShip { Position = new Position { StartRow = 2, EndRow = 2, StartColumn = 3, EndColumn = 6 }, Width = 4 });
            _boardRepository.GetBoard(_playerId).Returns(Task.FromResult(board));

            var boardService = new BoardService(_boardRepository);
            var result = await boardService.Attack(_playerId, new AttackRequest { Row = row, Column = column });
            result.Should().NotBeNull();
            result.IsHit.Should().Be(expectedHit);
            await _boardRepository.Received(1).GetBoard(_playerId);
            _boardRepository.Received(1).UpdateBoard(_playerId, board);
        }

        [Fact]
        public async Task attack_should_throw_board_not_found_if_no_board()
        {
            _boardRepository.GetBoard(_playerId).Returns(Task.FromResult((Board)null));

            var boardService = new BoardService(_boardRepository);
            Func<Task> addAction = async () => await boardService.Attack(_playerId, new AttackRequest { Row = 1, Column = 3 });
            addAction.Should().Throw<BoardDoesNotExistsException>();
            await _boardRepository.Received(1).GetBoard(_playerId);
        }

        [Fact]
        public async Task attack_should_throw_no_battleship_exception_when_board_does_not_have_ships()
        {
            var board = new Board(_playerId);
            _boardRepository.GetBoard(_playerId).Returns(Task.FromResult(board));

            var boardService = new BoardService(_boardRepository);
            Func<Task> addAction = async () => await boardService.Attack(_playerId, new AttackRequest { Row = 1, Column = 3 });
            addAction.Should().Throw<NoBattleshipsOnBoardException>();
            await _boardRepository.Received(1).GetBoard(_playerId);
        }

        [Fact]
        public async Task attack_should_attack_with_ship_sunk_and_game_over()
        {
            var board = new Board(_playerId);
            board.Battleships.Add(new BattleShip { Position = new Position { StartRow = 2, EndRow = 2, StartColumn = 3, EndColumn = 4 }, Width = 2 });
            _boardRepository.GetBoard(_playerId).Returns(Task.FromResult(board));

            var boardService = new BoardService(_boardRepository);
            var result = await boardService.Attack(_playerId, new AttackRequest { Row = 2, Column = 3 });
            result.Should().NotBeNull();
            result.IsHit.Should().Be(true);
            result = await boardService.Attack(_playerId, new AttackRequest { Row = 2, Column = 3 });
            result.Should().NotBeNull();
            result.IsHit.Should().BeTrue();
            result.IsShipSunk.Should().BeTrue();
            result.IsGameOver.Should().BeTrue();
            await _boardRepository.Received(2).GetBoard(_playerId);
            _boardRepository.Received(1).UpdateBoard(_playerId, board);
            _boardRepository.Received(1).RemoveBoard(_playerId);
        }
    }
}
