﻿using Battleship.Api.Tracker.Controllers;
using Battleship.Api.Tracker.Dtos;
using Battleship.Api.Tracker.Services;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Battleship.Api.Tracker.UnitTests.Controllers
{
    public class BoardControllerTests
    {
        private readonly IBoardService _boardService;
        private const int _playerId = 1;
        private Guid _boardId = Guid.NewGuid();
        private Guid? _shipId = Guid.NewGuid();

        public BoardControllerTests()
        {
            _boardService = Substitute.For<IBoardService>();
        }

        [Fact]
        public async Task create_board_should_create_board()
        {
            _boardService.CreateBoard(_playerId).Returns(Task.FromResult(_boardId));

            var boardController = new BoardsController(_boardService);
            var result = await boardController.CreateBoard(_playerId);
            result.Should().BeOfType<OkObjectResult>();

            var response = result as OkObjectResult;
            response.Value.Should().Be(_boardId);
        }

        [Fact]
        public async Task add_battleship_should_add_battleship()
        {
            _boardService.AddBattleship(_playerId, Arg.Any<AddBattleShipRequest>()).Returns(Task.FromResult(_shipId.Value));

            var boardController = new BoardsController(_boardService);
            var result = await boardController.AddBattleship(_playerId, new AddBattleShipRequest());
            result.Should().BeOfType<OkObjectResult>();

            var response = result as OkObjectResult;
            response.Value.Should().Be(_shipId.Value);
        }

        [Fact]
        public async Task attack_should_attack_battleship()
        {
            var attackReposne = new AttackResponse { IsHit = true, IsShipSunk = true, IsGameOver = true };
            _boardService.Attack(_playerId, Arg.Any<AttackRequest>()).Returns(Task.FromResult(attackReposne));

            var boardController = new BoardsController(_boardService);
            var result = await boardController.Attack(_playerId, new AttackRequest());
            result.Should().BeOfType<OkObjectResult>();

            var response = result as OkObjectResult;
            response.Value.Should().BeEquivalentTo(attackReposne);
        }

        [Fact]
        public async Task remove_should_remove_board_for_player()
        {
            var boardController = new BoardsController(_boardService);
            boardController.Remove(_playerId);
            _boardService.Received(1).RemoveBoard(_playerId);
        }
    }
}
