using Battleship.Api.Tracker.Entities;
using Battleship.Api.Tracker.Providers;
using Battleship.Api.Tracker.Repositories;
using FluentAssertions;
using NSubstitute;
using System.Threading.Tasks;
using Xunit;

namespace Battleship.Api.Tracker.UnitTests.Repositories
{
    public class BoardRepositoryTests
    {
        private readonly ICacheProvider _cacheProvider;
        private const int _playerId = 1;
        private IBoardRepository _boardRepository;

        public BoardRepositoryTests()
        {
            _cacheProvider = Substitute.For<ICacheProvider>();
            _boardRepository = new BoardRepository(_cacheProvider);
        }

        [Fact]
        public void create_board_should_create_board_if_not_exists()
        {
            _cacheProvider.Get<Board>(_playerId.ToString()).Returns((Board)null);

            var boardId = _boardRepository.CreateBoard(_playerId);

            boardId.Should().NotBeNull();
            _cacheProvider.Received(1).Get<Board>(_playerId.ToString());
            _cacheProvider.ReceivedWithAnyArgs(1).Set(_playerId.ToString(), Arg.Any<Board>());
        }

        [Fact]
        public async Task create_board_should_return_board_if_already_exists()
        {
            var board = new Board(_playerId);
            _cacheProvider.Get<Board>(_playerId.ToString()).Returns(board);

            var boardId = await _boardRepository.CreateBoard(_playerId);

            boardId.Should().NotBeEmpty();
            boardId.Should().Be(board.BoardId);
            _cacheProvider.Received(1).Get<Board>(_playerId.ToString());
            _cacheProvider.ReceivedWithAnyArgs(0).Set(_playerId.ToString(), Arg.Any<Board>());
        }

        [Fact]
        public async Task add_battleship_should_add_battleship_on_board()
        {
            var board = new Board(_playerId);
            var battleShip = new BattleShip { Width = 2, Position = new Position { StartRow = 1, StartColumn = 1, EndRow = 1, EndColumn = 5 } };
            _cacheProvider.Get<Board>(_playerId.ToString()).Returns(board);

            var boardId = await _boardRepository.AddBattleShip(_playerId, battleShip);

            boardId.Should().NotBeNull();
            board.Battleships.Count.Should().Be(1);
            _cacheProvider.Received(1).Get<Board>(_playerId.ToString());
        }

        [Fact]
        public async Task add_battleship_should_not_add_battleship_if_no_board()
        {
            var battleShip = new BattleShip { Width = 2, Position = new Position { StartRow = 1, StartColumn = 1, EndRow = 1, EndColumn = 5 } };
            _cacheProvider.Get<Board>(_playerId.ToString()).Returns((Board)null);

            var boardId = await _boardRepository.AddBattleShip(_playerId, battleShip);

            boardId.Should().BeNull();
            _cacheProvider.Received(1).Get<Board>(_playerId.ToString());
        }

        [Fact]
        public async Task get_board_should_get_board()
        {
            var board = new Board(_playerId);
            _cacheProvider.Get<Board>(_playerId.ToString()).Returns(board);

            var result = await _boardRepository.GetBoard(_playerId);

            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(board);
            _cacheProvider.Received(1).Get<Board>(_playerId.ToString());
        }

        [Fact]
        public void update_board_should_update_board()
        {
            var board = new Board(_playerId);
            board.Battleships.Add(new BattleShip { Width = 2, Position = new Position { StartRow = 1, StartColumn = 1, EndRow = 1, EndColumn = 5 } });

            _boardRepository.UpdateBoard(_playerId, board);
            _cacheProvider.Received(1).Set(_playerId.ToString(), board);
        }

        [Fact]
        public void delete_board_should_delete_board()
        {
            var board = new Board(_playerId);
            _cacheProvider.Get<Board>(_playerId.ToString()).Returns(board);

            _boardRepository.RemoveBoard(_playerId);
            _cacheProvider.Received(1).Remove(_playerId.ToString());
        }
    }
}
