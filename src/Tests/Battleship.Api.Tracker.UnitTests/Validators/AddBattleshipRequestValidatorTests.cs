﻿using Battleship.Api.Tracker.Dtos;
using Battleship.Api.Tracker.Validators;
using FluentAssertions;
using Xunit;

namespace Battleship.Api.Tracker.UnitTests.Validators
{
    public class AddBattleshipRequestValidatorTests
    {
        private readonly AddBattleshipRequestValidator _addBattleshipRequestValidator;
        public AddBattleshipRequestValidatorTests()
        {
            _addBattleshipRequestValidator = new AddBattleshipRequestValidator();
        }

        [Theory]
        [InlineData(1, 5, 3, Direction.Horizontal)]
        [InlineData(3, 1, 5, Direction.Vertial)]
        public void valid_request(int startRow, int startColumn, int width, Direction direction)
        {
            var addBattleshipRequest = new AddBattleShipRequest
            {
                Direction = direction,
                StartRow = startRow,
                StartColumn = startColumn,
                Width = width
            };

            var result = _addBattleshipRequestValidator.Validate(addBattleshipRequest);
            result.IsValid.Should().BeTrue();
            result.Errors.Count.Should().Be(0);
        }

        [Theory]
        [InlineData(0, 0, 0, Direction.Horizontal, 3)]
        [InlineData(0, -1, 0, Direction.Vertial, 3)]
        [InlineData(1, 9, 5, Direction.Horizontal, 1)]
        [InlineData(4, 6, 7, Direction.Vertial, 1)]
        public void Invalid_request(int startRow, int startColumn, int width, Direction direction, int expectedErrorCount)
        {
            var addBattleshipRequest = new AddBattleShipRequest
            {
                Direction = direction,
                StartRow = startRow,
                StartColumn = startColumn,
                Width = width
            };

            var result = _addBattleshipRequestValidator.Validate(addBattleshipRequest);
            result.IsValid.Should().BeFalse();
            result.Errors.Count.Should().Be(expectedErrorCount);
        }
    }
}
