﻿using Battleship.Api.Tracker.Dtos;
using Battleship.Api.Tracker.Validators;
using FluentAssertions;
using Xunit;

namespace Battleship.Api.Tracker.UnitTests.Validators
{
    public class AttackRequestValidatorTests
    {
        private readonly AttackRequestValidator _attackRequestValidator;
        public AttackRequestValidatorTests()
        {
            _attackRequestValidator = new AttackRequestValidator();
        }

        [Theory]
        [InlineData(1, 5)]
        [InlineData(3, 1)]
        public void valid_request(int row, int column)
        {
            var addBattleshipRequest = new AttackRequest
            {
               Row = row,
               Column = column
            };

            var result = _attackRequestValidator.Validate(addBattleshipRequest);
            result.IsValid.Should().BeTrue();
            result.Errors.Count.Should().Be(0);
        }

        [Theory]
        [InlineData(0, 0, 2)]
        [InlineData(0, -1, 2)]
        [InlineData(1, 19, 1)]
        [InlineData(14, 6, 1)]
        public void Invalid_request(int row, int column, int expectedErrorCount)
        {
            var addBattleshipRequest = new AttackRequest
            {
                Row = row,
                Column = column
            };

            var result = _attackRequestValidator.Validate(addBattleshipRequest);
            result.IsValid.Should().BeFalse();
            result.Errors.Count.Should().Be(expectedErrorCount);
        }
    }
}
