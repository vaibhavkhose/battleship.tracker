﻿namespace Battleship.Api.Tracker.Dtos
{
    public class AttackRequest
    {
        public int Row { get; set; }
        public int Column { get; set; }
    }
}
