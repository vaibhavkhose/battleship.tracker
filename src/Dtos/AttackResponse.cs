﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Api.Tracker.Dtos
{
    public class AttackResponse
    {
        public bool IsHit { get; set; }
        public bool IsShipSunk { get; set; }
        public bool IsGameOver { get; set; }
    }
}
