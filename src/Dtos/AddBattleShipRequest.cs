﻿namespace Battleship.Api.Tracker.Dtos
{
    public class AddBattleShipRequest
    {
        public int Width { get; set; }
        public int StartRow { get; set; }
        public int StartColumn { get; set; }
        public Direction Direction { get; set; }
    }

    public enum Direction
    {
        Horizontal,
        Vertial
    }
}