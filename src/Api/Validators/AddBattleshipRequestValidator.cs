﻿using Battleship.Api.Tracker.Dtos;
using FluentValidation;

namespace Battleship.Api.Tracker.Validators
{
    public class AddBattleshipRequestValidator : AbstractValidator<AddBattleShipRequest>
    {
        public AddBattleshipRequestValidator()
        {
            RuleFor(request => request.StartRow).GreaterThan(0).LessThanOrEqualTo(10);
            RuleFor(request => request.StartColumn).GreaterThan(0).LessThanOrEqualTo(10);
            RuleFor(request => request.Width).GreaterThan(0).LessThanOrEqualTo(10);
            When(request => request.Direction == Direction.Horizontal, () => {
                RuleFor(request => request.StartColumn + request.Width).LessThanOrEqualTo(10).WithMessage("Battleship test can't fit the board");
            }).Otherwise(() => {
                RuleFor(request => request.StartRow + request.Width).LessThanOrEqualTo(10).WithMessage("Battleship can't fit the board");
            });
        }
    }
}
