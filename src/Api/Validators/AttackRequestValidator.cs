﻿using Battleship.Api.Tracker.Dtos;
using FluentValidation;

namespace Battleship.Api.Tracker.Validators
{
    public class AttackRequestValidator : AbstractValidator<AttackRequest>
    {
        public AttackRequestValidator()
        {
            RuleFor(request => request.Row).GreaterThan(0).LessThanOrEqualTo(10);
            RuleFor(request => request.Column).GreaterThan(0).LessThanOrEqualTo(10);
        }
    }
}
