﻿namespace Battleship.Api.Tracker.Entities
{
    public class Position
    {
        public int StartRow { get; set; }
        public int EndRow { get; set; }
        public int StartColumn { get; set; }
        public int EndColumn { get; set; }
    }
}