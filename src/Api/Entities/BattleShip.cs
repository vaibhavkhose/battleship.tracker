﻿using System;

namespace Battleship.Api.Tracker.Entities
{
    public class BattleShip
    {
        public BattleShip()
        {
            ShipId = Guid.NewGuid();
        }
        public Guid ShipId { get; }
        public Position Position { get; set; }
        public int Width { get; set; }
        public int Hits { get; set; }
        public bool IsSunk => Hits >= Width;
    }
}
