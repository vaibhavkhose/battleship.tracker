﻿using System;
using System.Collections.Generic;

namespace Battleship.Api.Tracker.Entities
{
    public class Board
    {
        public Board(int playerId)
        {
            Rows = 10;
            Columns = 10;
            Battleships = new List<BattleShip>();
            PlayerId = playerId;
            BoardId = Guid.NewGuid();
        }
        public int Rows { get; }
        public int Columns { get; }
        public List<BattleShip> Battleships { get; set; }
        public int PlayerId { get; set; }
        public Guid BoardId { get; set; }
        public bool IsGameOver => Battleships.TrueForAll(ship => ship.IsSunk);
    }
}
