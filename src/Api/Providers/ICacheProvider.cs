﻿namespace Battleship.Api.Tracker.Providers
{
    public interface ICacheProvider
    {
        void Set<T>(string key, T value);

        T Get<T>(string key);

        void Remove(string key);
    }
}
