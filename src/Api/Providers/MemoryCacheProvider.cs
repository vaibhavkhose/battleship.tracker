﻿using System;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;

namespace Battleship.Api.Tracker.Providers
{
    public class MemoryCacheProvider : ICacheProvider
    {
        private readonly IMemoryCache _cache;
        private readonly IConfiguration _config;

        public MemoryCacheProvider(IMemoryCache cache, IConfiguration config)
        {
            _cache = cache;
            _config = config;
        }

        public void Set<T>(string key, T value)
        {
            var entry = _cache.CreateEntry(key);
            entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(Convert.ToDouble(_config["cacheInterval"]));
            _cache.Set(key, value);
        }

        public T Get<T>(string key)
        {
            _cache.TryGetValue(key, out T value);
            return value;
        }

        public void Remove(string key)
        {
            if (_cache.TryGetValue(key, out var value))
            {
                _cache.Remove(key);
            }
        }
    }
}
