﻿using System.Threading.Tasks;
using Battleship.Api.Tracker.Dtos;
using Battleship.Api.Tracker.Services;
using Microsoft.AspNetCore.Mvc;

namespace Battleship.Api.Tracker.Controllers
{
    [ApiController]
    public class BoardsController : ControllerBase
    {
        private readonly IBoardService _boardService;
        public BoardsController(IBoardService boardService)
        {
            _boardService = boardService;
        }

        [HttpPost]
        [Route("api/{playerId}/board")]
        public async Task<IActionResult> CreateBoard(int playerId)
        {    
            return Ok(await _boardService.CreateBoard(playerId));
        }

        [HttpPost]
        [Route("api/{playerId}/board/battleship")]
        public async Task<IActionResult> AddBattleship(int playerId, AddBattleShipRequest addBattleShipRequest)
        {
            return Ok(await _boardService.AddBattleship(playerId, addBattleShipRequest));
        }

        [HttpPost]
        [Route("api/{playerId}/board/attack")]
        public async Task<IActionResult> Attack(int playerId, AttackRequest attackRequest)
        {
            return Ok(await _boardService.Attack(playerId, attackRequest));
        }

        [HttpDelete]
        [Route("api/{playerId}/board")]
        public IActionResult Remove(int playerId)
        {
            _boardService.RemoveBoard(playerId);
            return NoContent();
        }
    }
}
