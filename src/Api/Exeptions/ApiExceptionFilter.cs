﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Battleship.Api.Tracker.Exeptions
{
    public class ApiExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is ApiException)
            {
                var exception = context.Exception as ApiException;
                context.ExceptionHandled = true;
                HttpResponse response = context.HttpContext.Response;
                response.StatusCode = (int)exception.StatusCode;
                response.ContentType = "application/json";
                context.Result = new ObjectResult(new { Message = exception.ErrorMessage });
            }
        }
    }
}
