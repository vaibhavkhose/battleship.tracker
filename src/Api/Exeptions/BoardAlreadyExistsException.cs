﻿using System.Net;

namespace Battleship.Api.Tracker.Exeptions
{
    public class BoardAlreadyExistsException : ApiException
    {
        public override HttpStatusCode StatusCode => HttpStatusCode.BadRequest;
        public override string ErrorMessage => "Board already exists for this player";
    }
}