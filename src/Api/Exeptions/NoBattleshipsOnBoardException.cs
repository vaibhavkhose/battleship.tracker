﻿using System.Net;

namespace Battleship.Api.Tracker.Exeptions
{
    public class NoBattleshipsOnBoardException : ApiException
    {
        public override HttpStatusCode StatusCode => HttpStatusCode.BadRequest;
        public override string ErrorMessage => "No battleship on the board to take attack";
    }
}