﻿using System;
using System.Net;

namespace Battleship.Api.Tracker.Exeptions
{
    public abstract class ApiException : Exception
    {
        public abstract HttpStatusCode StatusCode { get; }
        public abstract string ErrorMessage { get; }
    }
}
