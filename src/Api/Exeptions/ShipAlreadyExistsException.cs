﻿using System.Net;

namespace Battleship.Api.Tracker.Exeptions
{
    public class ShipAlreadyExistsException : ApiException
    {
        public override HttpStatusCode StatusCode => HttpStatusCode.BadRequest;
        public override string ErrorMessage => "Ship already present at this position";
    }
}