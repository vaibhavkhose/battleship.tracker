﻿using System.Net;

namespace Battleship.Api.Tracker.Exeptions
{
    public class BoardDoesNotExistsException : ApiException
    {
        public override HttpStatusCode StatusCode => HttpStatusCode.BadRequest;
        public override string ErrorMessage => "Board does not exists for this player";
    }
}