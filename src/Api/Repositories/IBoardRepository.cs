﻿using Battleship.Api.Tracker.Entities;
using System;
using System.Threading.Tasks;

namespace Battleship.Api.Tracker.Repositories
{
    public interface IBoardRepository
    {
        Task<Guid> CreateBoard(int playerId);
        Task<Guid?> AddBattleShip(int playerId, BattleShip ship);
        Task<Board> GetBoard(int playerId);
        void UpdateBoard(int playerId, Board board);
        void RemoveBoard(int playerId);
    }
}
