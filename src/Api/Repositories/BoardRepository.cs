﻿using Battleship.Api.Tracker.Entities;
using Battleship.Api.Tracker.Exeptions;
using Battleship.Api.Tracker.Providers;
using System;
using System.Threading.Tasks;

namespace Battleship.Api.Tracker.Repositories
{
    public class BoardRepository : IBoardRepository
    {
        private readonly ICacheProvider _cacheProvider;
        public BoardRepository(ICacheProvider cacheProvider)
        {
            _cacheProvider = cacheProvider;
        }

        public async Task<Guid> CreateBoard(int playerId)
        {
            var board = await GetBoard(playerId);
            if (board == null)
            {
                board = new Board(playerId);
                _cacheProvider.Set(playerId.ToString(), board);
            }
            return await Task.FromResult(board.BoardId);

        }
     
        public async Task<Guid?> AddBattleShip(int playerId, BattleShip battleShip)
        {
            var board = _cacheProvider.Get<Board>(playerId.ToString());
            if (board != null)
            {
                board.Battleships.Add(battleShip);
                return await Task.FromResult(battleShip.ShipId);
            }
            return null;
        }

        public async Task<Board> GetBoard(int playerId)
        {
            return await Task.FromResult(_cacheProvider.Get<Board>(playerId.ToString()));
        }

        public void UpdateBoard(int playerId, Board board)
        {
            _cacheProvider.Set(playerId.ToString(), board);
        }

        public void RemoveBoard(int playerId)
        {
            _cacheProvider.Remove(playerId.ToString());
        }
    }
}
