﻿using Battleship.Api.Tracker.Dtos;
using Battleship.Api.Tracker.Entities;
using Battleship.Api.Tracker.Exeptions;
using Battleship.Api.Tracker.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Battleship.Api.Tracker.Services
{
    public class BoardService : IBoardService
    {
        private readonly IBoardRepository _boardRepository;
        public BoardService(IBoardRepository boardRepository)
        {
            _boardRepository = boardRepository;
        }

        public async Task<Guid> CreateBoard(int playerId)
        {
            await CanCreateBoard(playerId);

            return await _boardRepository.CreateBoard(playerId);
        }

        public async Task<Guid> AddBattleship(int playerId, AddBattleShipRequest addBattleshipRequest)
        {
            await CanAddBattleshipOnBoard(playerId, addBattleshipRequest);

            var boardId = await _boardRepository.AddBattleShip(playerId, new BattleShip
            {
                Position = GeneratePosition(addBattleshipRequest),
                Width = addBattleshipRequest.Width
            });

            if(boardId == null)
            {
                throw new BoardDoesNotExistsException();
            }

            return boardId.Value;
        }

        public async Task<AttackResponse> Attack(int playerId, AttackRequest attackRequest)
        {
            var response = new AttackResponse();
            var board = await _boardRepository.GetBoard(playerId);

            CanAttack(board);

            foreach (var ship in board.Battleships)
            {
                if (!ship.IsSunk && IsShipHit(attackRequest, ship))
                {
                    response.IsHit = true;
                    ship.Hits += 1;
                    response.IsShipSunk = ship.IsSunk;
                }
            }

            response.IsGameOver = board.IsGameOver;

            UpdateBoardState(playerId, response.IsGameOver, board);

            return response;
        }

        public void RemoveBoard(int playerId)
        {
            _boardRepository.RemoveBoard(playerId);
        }

        private static void CanAttack(Board board)
        {
            if (board == null)
                throw new BoardDoesNotExistsException();

            if (!board.Battleships.Any())
                throw new NoBattleshipsOnBoardException();
        }

        private static bool IsShipHit(AttackRequest attackRequest, BattleShip ship)
        {
            return (attackRequest.Row >= ship.Position.StartRow && attackRequest.Row <= ship.Position.EndRow) && 
                   (attackRequest.Column >= ship.Position.StartColumn && attackRequest.Column <= ship.Position.EndColumn);
        }

        private Position GeneratePosition(AddBattleShipRequest addBattleShipRequest)
        {            
            return new Position
            {
                StartRow = addBattleShipRequest.StartRow,
                EndRow = addBattleShipRequest.Direction == Direction.Horizontal ? addBattleShipRequest.StartRow : addBattleShipRequest.StartRow + addBattleShipRequest.Width,
                StartColumn = addBattleShipRequest.StartColumn,
                EndColumn = addBattleShipRequest.Direction == Direction.Horizontal ? addBattleShipRequest.StartColumn + addBattleShipRequest.Width : addBattleShipRequest.StartColumn
            };          
        }

        private async Task CanCreateBoard(int playerId)
        {
            var board = await _boardRepository.GetBoard(playerId);
            if (board != null)
            {
                throw new BoardAlreadyExistsException();
            }
        }

        private async Task CanAddBattleshipOnBoard(int playerId, AddBattleShipRequest addBattleshipRequest)
        {
            var board = await _boardRepository.GetBoard(playerId);
            if (board == null)
            {
                throw new BoardDoesNotExistsException();
            }

            foreach (var existingShip in board.Battleships)
            {
                if (IsShipPresentOnRow(addBattleshipRequest, existingShip) || IsShipPresentOnColumn(addBattleshipRequest, existingShip))
                {
                    throw new ShipAlreadyExistsException();
                }              
            }
        }

        private static bool IsShipPresentOnColumn(AddBattleShipRequest addBattleshipRequest, BattleShip existingShip)
        {
            return addBattleshipRequest.Direction == Direction.Vertial &&
                   existingShip.Position.StartColumn == addBattleshipRequest.StartColumn &&
                   (addBattleshipRequest.StartRow >= existingShip.Position.StartRow || addBattleshipRequest.StartRow <= existingShip.Position.EndRow);
        }

        private static bool IsShipPresentOnRow(AddBattleShipRequest addBattleshipRequest, BattleShip existingShip)
        {
            return addBattleshipRequest.Direction == Direction.Horizontal &&
                   existingShip.Position.StartRow == addBattleshipRequest.StartRow &&
                   (addBattleshipRequest.StartColumn >= existingShip.Position.StartColumn || addBattleshipRequest.StartColumn <= existingShip.Position.EndColumn);
        }

        private void UpdateBoardState(int playerId, bool isGameOver, Board board)
        {
            if (isGameOver)
                _boardRepository.RemoveBoard(playerId);
            else
                _boardRepository.UpdateBoard(playerId, board);
        }
    }
}
