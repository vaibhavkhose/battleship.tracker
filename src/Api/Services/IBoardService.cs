﻿using Battleship.Api.Tracker.Dtos;
using System;
using System.Threading.Tasks;

namespace Battleship.Api.Tracker.Services
{
    public interface IBoardService
    {
        Task<Guid> AddBattleship(int playerId, AddBattleShipRequest addBattleshipRequest);
        Task<Guid> CreateBoard(int playerId);
        Task<AttackResponse> Attack(int playerId, AttackRequest attackRequest);
        void RemoveBoard(int playerId);
    }
}
