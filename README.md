# battleship.tracker

## This solution contains:
### API
    + POST api/{playerId}/board - Creates a 10 x 10 board for specified player
	  Returns error if board already exists for the player
	+ POST api/{playerId}/board/battleship - Add battleship to the board for specified player
	  Returns error if board not present for player or battleship already present at specified location on board
	+ POST api/{playerId}/board/attack - Attacked the battleship on the board for specified player.
	  Response indicates if attack is hit, is ship sunk and is game over
	  Returns error is board not present or no battleships added to board
	  

### Dtos
    + The data transfer objects (Request and Response)
    + This can be published to nuget and can be consumed by API consumers

### Unit Tests
    + Unit tests using xunit, NSubstitute and fluent assersions

### Framework and libraries used
    + .NET core 2.2
    + FluentValidation for request validation
    + Swashbuckle for swagger integration

## How to Run the application
### Visual studio - Run the API in IIS express (start without debugging)
### Command prompt - dotnet run

